const { indexOf, checkFields } = require('./index')

/* indexOf */
let data = [
  {
    test: [
      {
        mest: {
          gest: '123'
        }
      }
    ]
  },
  {
    test: [
      {
        mest: {
          gest: '321'
        }
      }
    ]
  }
]

console.log('[IndexOf] >>>', indexOf(data, { 'test.mest.gest': '321' }))
/* INDEXOF */

/* CHECKFIELDS */

const types = {
  bool: { type: 'Boolean' },
  name: {
    type: 'String'
  },
  phone: {
    type: 'String',
    required: true,
    reg: /[0-9]/
  },
  order: {
    type: 'Array',
    required: true,
    fields: {
      amount: { type: 'Number', min: 1, required: true },
      id: { type: 'Number', required: true }
    }
  },
  details: {
    type: 'Object',
    required: true,
    fields: {
      address: 'String',
      date: {
        type: 'Timestamp',
        required: true,
        filter: timestamp => timestamp > new Date().getTime()
      }
    }
  },
  message: 'String'
}

const body = {
  bool: false,
  name: null,
  phone: '+7 (777) 208 47 43',
  order: [
    {
      amount: 5,
      id: 31
    },
    {
      amount: 0,
      id: '32'
    },
    {
      amount: '36',
      id: 'wdawdaw'
    }
  ],
  details: {
    address: 'Мира 123',
    date: 1529783560071 // new Date('2019').getTime()
  },
  message: 'blablablabla'
}

const check = checkFields(body, types)
if (check.fail) {
  console.log('Fail')
}
console.log(JSON.stringify(check, null, 2))
