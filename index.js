/* CHECKFIELDS */

function dive (types, field = 'type') {
  let result = {}
  const keys = Object.keys(types)
  keys.forEach(key => {
    const filter = types[key]
    if (!filter || typeof filter === 'string') {
      result[key] = filter
    } else if (filter) {
      switch (filter.type) {
        case 'Array':
          if (filter.fields) {
            result[key] = [dive(filter.fields, field)]
          }
          break
        case 'Object':
          if (filter.fields) {
            result[key] = dive(filter.fields, field)
          }
          break
        default:
          result[key] = filter[field]
      }
    }
  })
  return result
}

const checkFields = (body, types) => {
  const errors = []
  const erroredFields = {}
  let errMsg = ''
  const fields = transform(body, types, true, (value, path, required, type, filters) => {
    const pathStr = path.reduce((path, item, index, arr) => {
      if (item.index >= 0) {
        path += '[' + item.index + ']'
      }
      if (index > 0) {
        path += '.'
      }
      path += item.key
      return path
    }, '')
    erroredFields[pathStr] = value
    if (required) {
      errors.push({ path: pathStr, value, type })
      if (filters) {
        errMsg += `[Обязательное поле] Неверное значение поля: ${pathStr}; Значение "${value}" не подходит по фильтрам.\n`
      } else {
        errMsg += `[Обязательное поле] Неверное значение поля: ${pathStr}; Ожидался [${type}] Пришел: ${value}.\n`
      }
    } else {
      if (filters) {
        errMsg += `[Предупреждение] Неверное значение поля: ${pathStr}; Значение "${value}" не подходит по фильтрам.\n`
      } else {
        errMsg += `[Предупреждение] Неверное значение поля: ${pathStr}; Ожидался [${type}] Пришел: ${value}.\n`
      }
    }
  })

  // if (required) { console.log(`Errored field at path: ${pathStr}; with value: ${value}`) }
  return {
    fields,
    fail: !!errors.length,
    erroredFields,
    accepting: dive(types, 'type'),
    description: dive(types, 'description'),
    errors,
    msg: errMsg
  }
}

function ch (v) {
  if ((typeof (v) === 'number' && isNaN(v)) || v === undefined || v === null) {
    return false
  }
  if (typeof (v) === 'boolean' || typeof (v) === 'number' || typeof (v) === 'string') {
    return true
  } else {
    return v
  }
}

const parseByType = (target, type) => {
  if (typeof target === 'undefined') {
    return target
  }
  let result
  switch (type) {
    case 'Number':
      if (isNaN(target * 1) || isNaN(parseInt(target))) {
        result = null
        break
      }
      result = parseInt(target)
      break
    case 'Double':
      if (isNaN(target * 1) || isNaN(parseFloat(target))) {
        result = null
        break
      }
      result = parseFloat(target)
      break
    case 'Timestamp':
      if (isNaN(target * 1) || isNaN(parseInt(target * 1)) || isNaN(new Date(target * 1).getTime())) {
        result = null
        break
      }
      result = new Date(parseInt(target)).getTime()
      break
    case 'Date':
      try {
        if (isNaN(new Date(target).getTime())) {
          result = null
          break
        }
        result = new Date(target)
      } catch (e) {
        result = null
      }
      break
    case 'Array':
      try {
        JSON.parse(target)
      } catch (e) {
        target = JSON.stringify(target)
      } finally {
        try {
          target = JSON.parse(target)
        } catch (e) {
          target = null
        } finally {
          result = target instanceof Array && Array.isArray(target)
            ? target
            : null
        }
      }
      break
    case 'Object':
      try {
        result = JSON.parse(target)
      } catch (e) {
        if (typeof target === 'object') {
          result = target
        } else {
          result = null
        }
      }
      break
    case 'Boolean':
      try {
        if (typeof result !== 'boolean') {
          result = JSON.parse(target)
        } else {
          result = target
        }
      } catch (e) {
        result = null
      }
      break
    case 'String':
      if (typeof result !== 'string') {
        try {
          result = target.toString()
        } catch (e) {
          result = null
        }
        break
      }
      result = target
      break
    default:
      result = target
  }

  return result
}

const checkEnum = (target, enumerator = [target]) => enumerator.reduce((result, en) => result || ((en instanceof RegExp) ? en.test(target) : en === target))

/*
  const filters = {
    type,
    reg,
    min,
    max,
    filter,
    maxLen,
    minLen
    description,
    enum
    fields: {}
  }
*/
function transform (targetObject, types, addNodes = true, errored = ((value, path, required, type, filters) => {}), path = [], index) {
  try {
    targetObject = JSON.parse(targetObject)
  } catch (e) {
    //
  }
  let result = {}
  const keys = Object.keys(types)
  keys.forEach(key => {
    const filter = types[key]
    if (!filter || typeof filter === 'string') {
      result[key] = parseByType(targetObject[key], filter)
      if (!ch(result[key])) {
        delete result[key]
        errored(targetObject[key], [...path, { key, index }], false, filter)
      }
    } else if (filter) {
      result[key] = parseByType(targetObject[key], filter.type)
      if (ch(result[key])) {
        let max = filter.max || filter.maxLen
        let min = filter.min || filter.minLen
        switch (filter.type) {
          case 'String':
            if (max && result[key].length > max) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (min && result[key].length < min) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.reg && !filter.reg.test(result[key])) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.filter && !filter.filter(result[key], key)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (!checkEnum(result[key], filter.enum)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            break
          case 'Number':
            if (filter.max && result[key] > filter.max) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.min && result[key] < filter.min) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.maxLen && result[key].length > filter.maxLen) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.minLen && result[key].length < filter.minLen) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.reg && !filter.reg.test(result[key])) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.filter && !filter.filter(result[key], key)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (!checkEnum(result[key], filter.enum)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            break
          case 'Array':
            if (max && result[key].length > max) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (min && result[key].length < min) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.filter && !filter.filter(result[key], key)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.fields && result[key]) {
              result[key] = result[key].map((item, i) => {
                return transform(item, filter.fields, true, errored, [...path, { key, index }], i)
              })
            }
            break
          case 'Object':
            if (filter.filter && !filter.filter(result[key], key)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.fields && result[key]) {
              result[key] = transform(result[key], filter.fields, true, errored, [...path, { key, index }])
            }
            break
          default:
            if (filter.filter && !filter.filter(result[key], key)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
            if (filter.enum && !checkEnum(result[key], filter.enum)) {
              delete result[key]
              errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
              break
            }
        }
      } else {
        delete result[key]
        errored(targetObject[key], [...path, { key, index }], filter.required, filter.type, true)
      }
    }
  })
  if (addNodes) {
    for (let key in types) {
      if (!result.hasOwnProperty(key)) {
        result[key] = undefined
      }
    }
  }
  return result
}
/* /CHECKFIELDS */

/* INDEXOF */

const getElementByPath = (element, path, callback, ind = 0) => {
  if (typeof path === 'string') {
    path = path.split('.')
  }
  const next = ind + 1
  if (next === path.length) {
    callback(element[path[ind]])
    return element[path[ind]]
  }
  if (Array.isArray(element[path[ind]])) {
    return element[path[ind]].reduce((err, item) => (getElementByPath(item, path, callback, next) || err), undefined)
  } else if ((element[path[ind]] && element[path[ind]][path[next]]) && (typeof element[path[ind]] === 'object')) {
    return getElementByPath((element[path[ind]]), path, callback, next)
  } else {
    return undefined
  }
}

const indexOf = (objArr, target) => {
  let result = -1
  const keys = Object.keys(target)
  objArr.forEach((obj, index) => {
    const found = keys.reduce((matches, field) => {
      const value = target[field]
      let path = field.split('.')
      getElementByPath(obj, path, (element) => {
        if (element !== value) {
          matches = false
        }
      })
      return matches
    }, true)
    if (found) {
      result = index
    }
  })
  return result
}

/* /INDEXOF */

const clean = (object, keysFrom) => {
  const keys = Object.keys(object)
  keysFrom = keysFrom || keys
  for (let i = 0; i < keys.length; i++) {
    if (!keysFrom.includes(keys[i]) || !ch(object[keys[i]])) {
      delete object[keys[i]]
    }
  }
}

module.exports = {
  ch,
  clean,
  indexOf,
  checkFields,
  getElementByPath
}
